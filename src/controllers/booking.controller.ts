import { Request, Response } from "express";
import { Booking } from "../entity/Booking";
import { Event } from "../entity/Event";

//OBTIENE TODOS LOS BOOKINGS
/**
 * La función `getBookings` recupera las reservas de la base de datos y las devuelve como una respuesta JSON.
 * @devuelve una respuesta JSON que contiene los datos de las reservas.
 */
export const getBookings = async (req: Request, res: Response) => {
  console.log('obteniendo bookings...');
  try {
    const booking = await Booking.find({
    relations: {
        user: true,
        event:true
    },
  });
    return res.json(booking);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

//OBTIENE UN BOOKING
/**
* La función `getBooking` es una función asíncrona que recupera una reserva basada en la información proporcionada
 * ID y lo devuelve como una respuesta JSON.
 * @devuelve una respuesta JSON que contiene el objeto de reserva si se encuentra. Si no se encuentra la reserva,
 * devuelve un código de estado 404 con un mensaje que indica que no se encontró la reserva. Si hay
 * un error durante el proceso, devuelve un código de estado 500 con un mensaje de error.
 */
export const getBooking = async (req: Request, res: Response) => {
  console.log('obteniendo un booking...');
  try {
    const { id } = req.params;
    const booking = await Booking.findOne({
      where: { id: parseInt(id)},
       relations: [ 'user','event']
      });
    if (!booking) return res.status(404).json({ message: "Booking not found" });
    return res.json(booking);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

//CREA UN BOOKING
/**
 * La función `createBooking` crea una nueva reserva para un usuario y evento, comprobando si el evento tiene
 * alcanzó su límite de reservas.
 * @returns El código devuelve una respuesta JSON con el objeto de reserva creado si la reserva es
 * creado con éxito. Si hay un error, devolverá una respuesta JSON con un mensaje de
 */
export const createBooking = async (req: Request, res: Response) => {
  console.log('creando booking...');
  try {
    const { id_user, id_event } = req.body;
    const eventAvailable = await Event.findOneBy({ id_event: parseInt(id_event) });
    console.log(eventAvailable?.limite)
    let limite = eventAvailable?.limite || 0
    let BookingUsed = await Booking.find({
        relations: {
        //user: true,
        event:true
        }})

    let bookingLimite=0
    for(let i=0;i<BookingUsed.length;i++){
      if(BookingUsed[i].event.id_event==id_event){
        bookingLimite++
      }
    }
    console.log(bookingLimite)
    if (bookingLimite>=limite) return res.status(404).json({ message: "exhausted reserves" });
    const booking = new Booking()
    booking.user=id_user;
    booking.event=id_event;
    await booking.save();
    return res.status(200).json(booking);
    
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

//ACTUALIZA UN BOOKING
/**
 * La función actualiza un registro de reserva en una base de datos según la identificación proporcionada.
 * @devuelve una respuesta con un código de estado y un objeto JSON. Si no se encuentra la reserva, devuelve un
 * Código de estado 404 con un mensaje que indica que no se encontró ninguna reserva. Si hay un error durante la
 * proceso de actualización, devuelve un código de estado 500 con un mensaje de error. Si la actualización es exitosa, se
 * devuelve un código de estado 204 
 */
export const updateBooking = async (req: Request, res: Response) => {
  console.log('actualizando booking')
  const { id } = req.params;
  const { id_user, id_event } = req.body;
  try {
    const booking = await Booking.findOneBy({ id: parseInt(id) });
    if (!booking) return res.status(404).json({ message: "Not booking found" });
    const bookingUpdated = new Booking()
    bookingUpdated.user=id_user;
    bookingUpdated.event=id_event;
    await Booking.update({ id: parseInt(id) }, bookingUpdated);
    return res.status(200).json({ message: "Booking updated satifactorily" });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

//ELIMINA UN BOOKING
/**
 * La función `deleteBooking` es una función asíncrona que elimina una reserva en función de la
 * ID proporcionado y devuelve una respuesta adecuada.
 * @devuelve una respuesta con un código de estado y un objeto JSON. Si no se encuentra la reserva, devuelve un
 * Código de estado 404 con mensaje "Reserva no encontrada". Si la reserva se elimina con éxito, se
 * devuelve un código de estado 204 con un mensaje "Reserva eliminada". Si hay un error, devuelve un 500
 * código de estado con el mensaje de error.
 */
export const deleteBooking = async (req: Request, res: Response) => {
  console.log('eliminando booking')
  const { id } = req.params;
  try {
    const result = await Booking.delete({ id: parseInt(id) });
    if (result.affected === 0)
      return res.status(404).json({ message: "Booking not found" });
    return res.status(200).json({ message: "Booking eliminado" });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};