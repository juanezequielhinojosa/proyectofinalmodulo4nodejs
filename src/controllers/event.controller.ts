import { Request, Response } from "express";
import { Event } from "../entity/Event";


//OBTIENE TODOS LOS EVENTOS
/**
* La función `getEvents` es una función asíncrona que recupera eventos de una base de datos y
 * los devuelve como una respuesta JSON.
 * @devuelve una respuesta JSON que contiene los datos de los eventos.
 */
export const getEvents = async (req: Request, res: Response) => {
  console.log('obteniendo los eventos...');
  try {
    const events = await Event.find();
    return res.json(events);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

//OBTIENE UN EVENTO 
/**
 * La función `getEvent` es una función asíncrona que recupera un evento por su ID y lo devuelve
 * como una respuesta JSON, o devuelve un mensaje de error si no se encuentra el evento o si hay un error interno
 * Error del Servidor.
 * @devuelve un código de estado 404 con una respuesta JSON que contiene el mensaje "Evento no encontrado". Si hay
 * un error durante el proceso, devuelve un código de estado 500 con una respuesta JSON que contiene el error
 * mensaje.
 */
export const getEvent = async (req: Request, res: Response) => {
  console.log('obteniendo un evento...');
  try {
    const { id } = req.params;
    const event = await Event.findOneBy({ id_event: parseInt(id) });
    if (!event) return res.status(404).json({ message: "Event not found" });
    return res.json(event);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

//CREA UN EVENTO
 /**
  * Esta función crea un evento extrayendo datos del cuerpo de la solicitud, creando un nuevo evento
  * objeto, y guardarlo en la base de datos.
  * @devuelve una respuesta JSON que contiene el objeto de evento creado.
  */
 export const createEvent = async (req: Request, res: Response) => {
  console.log('creando un evento...');
  try {
    const { type, name, descripcion, lugar, fechaHora, gps, limite, precio } = req.body;
    const event = new Event();
    event.type = type;
    event.name = name;
    event.descripcion = descripcion;
    event.lugar = lugar;
    event.fechaHora = fechaHora;
    event.gps = gps;
    event.limite = parseInt(limite);
    event.precio = parseInt(precio);
    await event.save();
    return res.json(event);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
  
};

//ACTUALIZA UN EVENTO
/**
 * Esta función actualiza un evento en una base de datos según la identificación proporcionada.
 * @devuelve una respuesta con un código de estado y un objeto JSON. Si no se encuentra el evento, devuelve un
 * Código de estado 404 con un mensaje que indica que no se encontró ningún evento. Si hay un error durante la
 * proceso de actualización, devuelve un código de estado 500 con un mensaje de error. Si la actualización es exitosa, se
 * devuelve un código de estado 204 
*/
export const updateEvent = async (req: Request, res: Response) => {
  console.log('actualizando evento')
  const { id } = req.params;
  try {
    const event = await Event.findOneBy({ id_event: parseInt(id) });
    if (!event) return res.status(404).json({ message: "Not event found" });
    await Event.update({ id_event: parseInt(id) }, req.body);
    return res.status(200).json({ message: "Evento actualizado" });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

//ELIMINA UN EVENTO
/**
 * The function deletes an event from the database and returns an appropriate response.
 * @devuelve una respuesta con un código de estado y un objeto JSON. Si el evento se elimina con éxito, se
 * devolverá un código de estado de 204 (Sin contenido) y un objeto JSON con el mensaje "Evento eliminado".
 * Si no se encuentra el evento, devolverá un código de estado de 404 (No encontrado) 
 */
export const deleteEvent = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const result = await Event.delete({ id_event: parseInt(id) });
    if (result.affected === 0)
      return res.status(404).json({ message: "Event not found" });
    return res.status(200).json({ message: "Event eliminado" });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};