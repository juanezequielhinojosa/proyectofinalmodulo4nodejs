 import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    BaseEntity,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
  } from "typeorm";
import  { Booking } from "./Booking"
  
  @Entity() 
  export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id_user: number;
  
    @Column()
    username: string;
  
    @Column()
    password: string;
  
    @Column({ default: true })
    active: boolean;
  
    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(() => Booking, (booking)=>booking.user)
    booking: Booking;

}
