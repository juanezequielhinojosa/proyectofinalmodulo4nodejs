import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    BaseEntity,
    CreateDateColumn,
    OneToMany,
  } from "typeorm";
  import  { Booking } from "./Booking"

  
  @Entity()
  export class Event extends BaseEntity {
    @PrimaryGeneratedColumn()
    id_event: number;
  
    @Column()
    type: string;
  
    @Column()
    name: string;

    @Column()
    descripcion: string;

    @Column()
    lugar: string;
  
    @Column()
    fechaHora: string;

    @Column()
    gps: string;

    @Column()
    precio: number;

    @Column()
    limite: number;
  
    @CreateDateColumn()
    createdAt: Date;

    @OneToMany(() => Booking, (booking)=>booking.event)
    booking: Booking;
      
  }