import { Entity, 
  PrimaryGeneratedColumn, 
  JoinColumn, 
  CreateDateColumn, 
  ManyToOne, 
  BaseEntity } from "typeorm"
import { User } from "./User"
import { Event } from "./Event"

@Entity()
export class Booking extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdAt: Date;

  @ManyToOne(() => User)
  @JoinColumn({name:'id_user'})
  user: User;

  @ManyToOne(() => Event)
  @JoinColumn({name:'id_event'})
  event: Event;
}