import "reflect-metadata";
import app from './app';
import { AppDataSource } from "./db";
import {PORT} from './config'


/* El bloque de código es la función principal de la aplicación. Es responsable de inicializar el
conexión de la base de datos, registrando un mensaje cuando la conexión es exitosa e iniciando el
servidor de aplicaciones para escuchar en un puerto especificado. */
async function main() {
  try {
    await AppDataSource.initialize();
    console.log('Database Connected...');
    app.listen(PORT, () => console.log(`App listening on port ${PORT}!`))
  } catch (error) {
    console.error(error);
  }
}
main();