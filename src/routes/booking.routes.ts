import { Router } from "express";
import {
  getBookings,
  getBooking,
  createBooking,
  updateBooking,
  deleteBooking,
} from "../controllers/booking.controller";
import passport from 'passport'

const router = Router();
/* Estas líneas de código definen las rutas para manejar las solicitudes HTTP relacionadas con las reservas. Cada
ruta está asociada con un método HTTP específico (GET, POST, PUT, DELETE) y un método HTTP correspondiente
función del controlador. */
router.get("/bookings", passport.authenticate('jwt', { session: false }), getBookings);
router.post("/bookings", passport.authenticate('jwt', { session: false }),createBooking);
router.put("/bookings/:id", passport.authenticate('jwt', { session: false }),updateBooking);
router.delete("/bookings/:id", passport.authenticate('jwt', { session: false }), deleteBooking);

export default router;