import { Router } from "express";
import {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
} from "../controllers/user.controller";
import {signIn, signUp, protectedEndpoint, refresh } from '../controllers/user.controller'
import passport from 'passport'
const router = Router();


/* El código está definiendo varias rutas para manejar operaciones relacionadas con el usuario en un
Aplicación Express.js. Aquí hay un desglose de lo que hace cada ruta: */
router.get("/users", passport.authenticate('jwt', { session: false }), getUsers);
router.get("/users/:id", passport.authenticate('jwt', { session: false }), getUser);
router.post("/users/signup", createUser);
router.put("/users/:id", passport.authenticate('jwt', { session: false }), updateUser);
router.delete("/users/:id", passport.authenticate('jwt', { session: false }), deleteUser);
router.post('/users/signin', signIn);
router.post('/token', refresh);


export default router;