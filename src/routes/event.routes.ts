import { Router } from "express";
import {
  getEvents,
  getEvent,
  createEvent,
  updateEvent,
  deleteEvent,
} from "../controllers/event.controller";
import passport from 'passport'

const router = Router();

/* El bloque de código está definiendo las rutas para manejar diferentes
Solicitudes HTTP relacionadas con eventos. Aquí hay un desglose de lo que hace cada ruta: */
router.get("/events", getEvents);//no es necesario autenticarse
router.get("/events/:id", passport.authenticate('jwt', { session: false }),getEvent);
router.post("/events", passport.authenticate('jwt', { session: false }), createEvent);
router.put("/events/:id", passport.authenticate('jwt', { session: false }),updateEvent);
router.delete("/events/:id", passport.authenticate('jwt', { session: false }), deleteEvent);

export default router;