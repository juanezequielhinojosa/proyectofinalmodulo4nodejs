# Proyecto Final de Modulo 4: NodeJS :rocket:

 **Juan Ezequiel Hinojosa**

## Desarrollo 
Este proyecto consiste en el desarrollo de un servidor para un sistema backend de reserva de tickets

## Puntos Desarrollados :rocket:

La aplicación ofrece las siguientes caracteristicas:

- Mediante el uso de un emulador de cliente (POSTMAN), se pueden  realizar peticiones a distintos endpoints los cuales permiten persistir informacion en la base de datos.
- El servidor esta preparado para reacibir solicitudes de distintos tipos (POST, GET, PUT, DELETE).
## Importante :rocket:
- El servidor permite validar que cada evento tenga cupos disponibles antes de registrar la reserva.

Esperamos que el proyecto sea de su agrado, saludos.
